---
uuid:             c8c869b2-f5b5-4218-8b9a-ceaea779f6e4
layout:           post
title:            三次元世界
slug:             life-record
subtitle:         null
date:             '2014-11-27T09:00:00.000Z'
updated:          '2015-07-31T06:44:16.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags: null

---


无聊人为充实生活的努力，算是不务正业。

- [电影观赏](/tag/movies/)
- [读书笔记](/tag/reading/)
- [吃来喝去](/tag/foods/)
- [养花种草](/tag/plants/)
- [难以归类](/tag/3d-world)



