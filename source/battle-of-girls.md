---
uuid:             228e5405-e5f4-4469-b5f6-fa1793e989e8
layout:           post
title:            少女们的战争
slug:             battle-of-girls
subtitle:         null
date:             '2014-12-10T03:58:09.000Z'
updated:          '2015-07-31T06:50:12.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags: null

---



# 介绍

[同人创作灵感起源](/battlesofgirls-origin/ "少女们的战争 缘起")。

[设定](/battlesofgirls-defines/ "少女们的战争 当前已公开设定")。


# 目录

- 第一章 起源 - [第一节 圆神](/battlesofgirls-1-1/ "少女们的战争 第一章 第一节 圆神")
 - [第二节 焰魔与小圆](/battlesofgirls-1-2/ "少女们的战争 第一章 第二节 焰魔与小圆")
 - [第三节 菲特](/battlesofgirls-1-3/ "少女们的战争 第一章 第三节 菲特")
 - [第四节 奈叶](/battlesofgirls-1-4/ "少女们的战争 第一章 第四节 奈叶")
 - [第五节 菱川六花](/battlesofgirls-1-5/ "少女们的战争 第一章 第五节 菱川六花")
- 第二章 备战
 - [第一节 不靠谱的主从](/battlesofgirls-2-1/ "少女们的战争 第二章 第一节 不靠谱的主从")
 - [第二节 默契的主从](/battlesofgirls-2-2/ "少女们的战争 第二章 第二节 默契的主从")
 - [第三节 不合适的主从](/battlesofgirls-2-3/ "少女们的战争 第二章 第三节 不合适的主从")
 - [第四节 特殊的主从](/battlesofgirls-2-4/ "少女们的战争 第二章 第四节 特殊的主从")
- 第三章 初战
 - [第一节 她与他](/battlesofgirls-3-1/ "少女们的战争 第三章 第一节 她与他")
 - [第二节 不如愿](/battlesofgirls-3-2/ "少女们的战争 第三章 第二节 不如愿")
 - [第三节 命运的邂逅](/battlesofgirls-3-3/ "少女们的战争 第三章 第四节 危险的重逢")
 - [第四节 危险的重逢](/battlesofgirls-3-4/ "少女们的战争 第三章 第五节 神的救赎与愤怒")
 - [第五节 神的救赎与愤怒](/battlesofgirls-3-5/ "少女们的战争 第三章 第五节 神的救赎与愤怒")
 - [第六节 苦战](/battlesofgirls-3-6 "少女们的战争 第三章 第六节 苦战")
 - [第七节 真相？](/battlesofgirls-3-7 "少女们的战争 第三章 第七节 真相？")
- 第四章 心之历练
 - [第一节 Caster](/battlesofgirls-4-1/ "少女们的战争 第四章 第一节 Caster")
 - [第二节 决定](/battlesofgirls-4-2/ "少女们的战争 第四章 第二节 决定")
 - [第三节 光美奇迹](/battlesofgirls-4-3/ "少女们的战争 第四章 第三节 光美奇迹")
 - ....




