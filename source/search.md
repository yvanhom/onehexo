---
uuid:             18adaccc-5877-4f96-a230-3882c4d524b3
layout:           post
title:            'Search one.yvanhom.com'
slug:             search
subtitle:         null
date:             '2015-11-11T03:16:28.000Z'
updated:          '2015-11-11T03:20:36.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags: null

---

<form method="get" action="https://www.google.com/search">
    <input type="text" placeholder="Search..." name="q" value="" /> 
    <input type="hidden" name="sitesearch" value="one.yvanhom.com" />
</form>
