---
uuid:             603561a3-bcc9-4f42-9200-451a548f7465
layout:           post
title:            电影：室友
slug:             movie-roommate
subtitle:         null
date:             '2015-09-04T21:56:54.000Z'
updated:          '2015-09-04T21:56:54.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

日本电影，人格分裂，[豆瓣链接](http://movie.douban.com/subject/24736793/)

精神病人，惹不起啊。

童年阴影太恐怖了。

给少女造成童年阴影的人渣更不可饶恕。

人格分裂，多重人格的电影，最精彩的部分便是猜测有多少人格，出现的人物中哪些是真实的，哪些又是人格分裂出来的。

不过这部电影，看着感觉很无聊，虽然最后的展开很有意思，出乎意料，但也只是最后，前边太无聊，不过没有前边的无聊铺垫，也体现不出最后的精彩，可是，前边还是太无聊了。

男主不出彩，最好不要。

昨天看了四部电影，在 bilibili 上看的，都贴上了百合标签，最喜欢的是脑浆炸裂少女，之后是带吸管的玛格丽塔，一般的室友，无聊的梦径。
