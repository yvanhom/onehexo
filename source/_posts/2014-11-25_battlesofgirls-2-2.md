---
uuid:             61ac48c3-596e-4333-98d7-d9cc0c4e503f
layout:           post
title:            '少女们的战争 第二章 第二节 默契的主从'
slug:             battlesofgirls-2-2
subtitle:         null
date:             '2014-11-25T12:00:00.000Z'
updated:          '2015-07-31T06:43:27.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world
  - fanfiction
  - battlesofgirls

---


一日，天色已晚，路上行人也相应变得稀少，这使得在路上行走的两个女孩显得特别突出。

走在前方的还是小学生大小，留着高贵的银色头发，穿着少有的紫色贵族服装，戴着小紫帽，显得活泼可爱，那双红宝石般的眼睛令人怜爱。

这么晚，这么可爱的小学生在外边行走，会吸引好心人上前询问是否迷路。

可是没人会这么做，因为紧跟在后边的虽然只是个比小学生略大的女孩，但是只要略微靠近，便让人毛骨悚然，就像电影中的鬼娃娃，感觉只要再靠近，就会像电影中的悲剧路人一样被恶毒地诅咒。

白色的斗篷遮盖住了她的脸，只能从身形上看出是女孩，年龄估摸只有中学生大小，斗篷下露出的那双映着血色光芒的双眼，让人怎么看都像是现世的女厉鬼。

两人一前一后地走着，给人感觉怎么都不协调，又感觉非常默契。

走在前边的女孩对路人异样的眼光浑然不觉，因为她现在有一件很开心的事去做，开心到有时候还会蹦蹦跳跳起来。

她是依莉雅苏菲尔·冯·爱因兹贝伦，强大的魔术师，为圣杯而改造的魔术师。

自从十年前父母离开了她，她便很少这么开心过，也没有像现在这么自由过。

在苦难与孤独中成长，没有一天被当作人看待，更没有被当成过小孩子。对养大她的人来说，她只是物品，工具，随时可以替代，也随时可以丢弃。

父母没有来拯救她。

母亲已经死去，在大约十年前的时候。血浓于水，她感觉得到，有时还能够听到母亲在梦中跟她谈话，可是当她要开心地去拥抱时，母亲便离开了，消失不见了。

父亲，则因为有了另一个孩子，一个没有血缘关系的男孩子而背叛家族，抛弃了她，置她于不顾。

不可原谅。

可是父亲最后也死了，她没感觉到，是由爱因兹贝伦的人告诉她的。

还没有好好说爱你，怎么能就这么死了呢！

不过还好，那个在父亲关爱中长大的男孩还活着。

她现在就要去把对父亲的爱给予这个人，让他离开这个苦难的人间，跟父母重聚。

不知不觉，卫宫家的房子就在眼前，她感觉到她的Servent，无敌的Berserker已经按耐不住，狂化的力量需要宣泄。

房子的主人卫宫士郎此时却浑然不觉，刚刚送走老师和樱的他正在收拾着屋子。

可伊莉雅明显不理会士郎爱屋子的心情，所以当她的从者取出可爱的法杖宝具一炮把大门，外加庭院轰个稀巴烂时，也没出声阻止。

惊闻异变的卫宫士郎手上拿着投影出来的钢管冲出来，看到的却是一个可爱的萝莉，不禁不好意思的把钢条往身后藏。

“你好，大哥哥。”

“你好…”

白色斗篷的Berserker没有等士郎打完招呼，便挥动法杖，瞬时身体周围悬浮着多个光能量球，从中释放出能量，而攻击的目标便是卫宫士郎。

“好好玩哦，大哥哥。”

被攻击的卫宫士郎，敏捷地作出闪避，可是下一轮的炮击已经赶到，不给他休息的机会，只能迅速地躲回屋子里寻找掩体。

Berserker并没有移动，只是改变能量球的攻击方向，而方向所指，一面面墙应声被破坏，暴露出躲在后边的士郎。

Berserker不满地发出呼呼声，因为她渴望的是战斗，而不是约束力量玩游戏。

游戏中的卫宫士郎可没有一丝享受心情，投影出来的铁板作用不大，一被炮火攻击就消失，完全是螳臂当车，而且在密集的炮火下，不禁要小心的躲避，还要担心可能会倒塌的房屋，闪逃中的他显得非常的狼狈。

逃跑中的卫宫士郎不知不觉来到训练室，无路可退。

“真遗憾，这么快就玩完了。”再开心的事也总有结束的时候，依莉雅表示已经玩够了。

“再见，大哥哥。”

应声，Berserker周围的能量球汇集成一个，然后向卫宫士郎炮击。

他已经无路可逃，无物可挡，只能等着被炸毁，只能祈祷。

可是就像是上天听到了他的祈祷，在这危急的关头眷顾他，魔术阵的光芒照亮了整个训练室，而魔阵的中间隐约站着上天派来的使者，将要毁掉他的攻击挡了下来。

能量球的爆炸激起又一阵耀眼的强光，带来空气的激荡，尘土的飞扬，他的视线模糊，他只感觉到手臂像被灼烧般刺痛。

而在外边看戏的依莉雅则不受影响，看着突然到来的从者，卫宫士郎的从者，若有所思。

Berserker的斗篷因为战斗而脱落，露出了她面目，秋色头发，可爱的双马尾，系着黑色发带，精致的脸蛋，跟散发着诡异气息，泛着血光的双眼等女厉鬼形象格格不入。

致命的攻击被挡住，到来的是实力不错的对手，Berserker发出了兴奋的吼声，准备发起真正的攻击。

“就这样吧，Berserker，因为已经说再见了。”

对着倒在地方的卫宫士郎，依莉雅发出了新的拜访请求。

“那么，大哥哥，下次我再来玩，记得准备一下。”

让他拥有点希望，然后再让他绝望，依莉雅这十年来就是这么过来的，不断的期望父亲会来找她，期待别人来救她，期望别人会怜悯她，可是什么都没有。

下次，一定会让他绝望。

她对Berserker非常自信，那是最强的Barserker，是最强的从者，所以她无惧卫宫士郎的从者。

Barserker的召唤会附带狂化，以失去理智为代价，大幅度地提升力量，耐久，敏捷，给人的印象，一般是体型巨大的近战肌肉型英灵。

而她的Berserker不同，远程炮击魔导师，狂化让她的魔力大大增强，破坏力加大，魔力恢复加快。

实在是完美的。

狂化带来英灵个人意识的缺失，不过伊莉雅与Berserker非同一般的灵魂联系，使得她能够看从者所看，听从者所听，甚至能够通过这个联系将治疗的魔力传达给从者。同时Berserker残留的一点意识，能够感知到Master所想，并绝对的贯彻执行。

这个联系使得她们的组合更加完美。

所以明天再来，明天一定会玩得更加开心。



