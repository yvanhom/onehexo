---
uuid:             a75e8044-797d-4e8d-ba84-a7caac0618e1
layout:           post
title:            深圳独行一日游
slug:             shenzhen-oneday-oneperson
subtitle:         null
date:             '2015-08-05T23:29:15.000Z'
updated:          '2016-07-04T08:17:18.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world

---

热火朝天来到深圳，天气确实好，阳光明媚，就是热了点。

本来计划先去世界之窗逛逛，在网上找票，发现是 120 块，这么贵，还是到场买票吧。于是风风火火地赶到现场，居然是啤酒节，可是我并不喜欢喝啤酒，所以也不兴奋，到售票处一看，居然要 180，亏了，晚场 60，还是晚上再来吧，这样亏得少一点。

![世界之窗大门](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-1.jpg)
![世界之窗大门喷泉](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-2.jpg)

先去逛别的地方，晚上再回来，Here 地图，将周围过了一遍，发现了个锦绣中华风俗村，于是前进，路过何香凝美术馆，可惜闭馆维修，明年才开，继续前进，居然看到海盗船，这住所不错。

![去锦绣中华的路上](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-3.jpg)
![去锦绣中华的路上-海盗船](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-4.jpg)

到达锦绣中华，看到四面神，大梵天？碰上泼水节，再次可惜，我可不想被泼一身，先在伞下椅子上休息休息。休息完去买票，汗，150，舍不得，再说我也不想参与泼水节，不值不值，算了。

![锦绣中华风俗村](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-5.jpg)
![锦绣中华风俗村2](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-6.jpg)

吸取教训，这次找免费公园。

首先是大沙河公园，没有地铁，只能跑到上边搭公交，下了公交，感慨深圳的绿化不错。

![去大沙河公园的路上](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-7.jpg)
![去大沙河公园的路上](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-8.jpg)
![去大沙河公园的路上](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-9.jpg)

走上大沙河公园的天桥，在天桥上拍一张蓝天白云，天气真好，看着地下穿梭的车辆，心有余悸，下了天桥，便是大沙河公园了。

![大沙河天桥](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-10.jpg)
![大沙河天桥所见](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-11.jpg)

大沙河公园，没什么景点，就是绿化比较好，适合散步，放风筝，我到的时间是下午一点，正是太阳当空的时候，于是公园内几乎一个人都没有，感受下一丝丝的凉风，真是太爽快了。公园内鸟不少，可惜比较怕我，我一靠近就跑了。

![大沙河公园1](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-12.jpg)
![大沙河公园2](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-13.jpg)
![大沙河公园3](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-14.jpg)
![大沙河公园4](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-15.jpg)
![大沙河公园5](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-16.jpg)
![大沙河公园6](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-17.jpg)
![大沙河公园7](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-18.jpg)
![大沙河公园8](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-19.jpg)
![大沙河公园9](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-20.jpg)
![大沙河公园10](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-21.jpg)
![大沙河公园11](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-22.jpg)

逛完大沙河公园，时间还早，于是决定再去南山公园。此时我的脚已经很不舒服，鞋子不合脚，各种磨，左脚已经破皮，两脚都有水泡，真恨不得光着脚走路，可是地面太热，怕怕不敢，只能硬撑了，感觉我的体力能够克服。

在公交上七转八折，终于来到了南山公园。爬山，我才意思到，太迟钝了，带山字的公园，当然是爬山了。

体力充足，就是脚不舒服，爬个小南山，没问题的，于是爬山。

南山，寿比南山。

![南山石壁文](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-23.jpg)
![南山石壁文](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-24.jpg)

南山并不高，爬起来并不困难，只是此时，我的脚已经被鞋子磨得近乎废了，于是爬到一半便想放弃，全靠意志支撑着，上南山，我选择的是比较陡的石阶路，这条道人比较少，爬起来也相对比较耗体力，汗水流了不少，累啊。

![爬南山1](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-25.jpg)
![爬南山2](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-26.jpg)
![爬南山3](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-27.jpg)
![爬南山4](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-28.jpg)
![爬南山5](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-29.jpg)
![爬南山6](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-30.jpg)
![爬南山7](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-31.jpg)
![爬南山8](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-32.jpg)
![爬南山9](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-33.jpg)
![爬南山10](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-34.jpg)
![爬南山11](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-36.jpg)
![爬南山12](http://o9sak9o6o.bkt.clouddn.com/sites/shenzhen-37.jpg)

辛辛苦苦爬上南山，下山又是一个大问题。脚已经废了，此时我只想剁了它们。不想回头，于是就从另一条登山道下山，这条比较平，不是石阶，是水泥斜坡，下斜坡，我的脚啊，又不敢跑起来，怕刹不住，只能一步一步地下山。此时的我只想赶紧回去休息，泡脚，什么都没拍，而且，手机已经快没电了。

下了山，是荔林公园，不熟悉，要出公园不容易，一不小心便被公园内散步的人带着绕圈，绕了近半个多小时才出去。实际可能没那么久，可是此时的我，每走一步都是身心的折磨。

出了公园，在沙县小吃里吃了晚餐，休息休息，便回家泡脚了，至于世界之窗的夜间场，以后有机会再去吧，现在没精力了，只想脱下一身臭臭的满是汗水的衣服，好好睡上一觉。
