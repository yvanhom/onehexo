---
uuid:             81c9d4c3-fcf8-408a-acac-402d4039cb0b
layout:           post
title:            终于比较成功了
slug:             well-after-all
subtitle:         null
date:             '2014-12-27T08:58:26.000Z'
updated:          '2016-07-04T07:33:16.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - foods

---


经过上次的失败，这次吸取教训，终于算是比较成功了。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/fanqiefan-2.jpg)

这次比上次增加了玉米粒，不错。

大吃两碗，之后加了不少老干妈，又猛吃了一晚，心满意足，特别撑。

不过汤却是失败了。

没有准备汤，于是煮了点燕麦片。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/yanmaipian.jpg)

可是麦片下多了，完全不好吃，浪费了不少，下次吸取教训。

此外，元宵节没吃汤圆，所以买了一些当宵夜，不错。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/tangyuan.jpg)



