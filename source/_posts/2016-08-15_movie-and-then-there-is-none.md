---
uuid:             cad3495b-97fc-4f51-a741-857da16dd084
layout:           post
title:            电影：无人生还
slug:             movie-and-then-there-is-none
subtitle:         null
date:             '2016-08-15T23:17:04.000Z'
updated:          '2016-08-15T23:17:04.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

原著很有名，很强大，这部三集的电视剧，也拍得很棒。

这不是日本的推理小说，会把线索列出来，会有人在中途进行推理。推理者是观看者，我不断地猜想，究竟这几个人中谁是凶手，或者岛上还有其他人？

而结局我也没想到，凶手确实在他们当中，而他居然是凶手。

这片，比起推理，更注重的还是心理，人物的心理变化，人在受高压下的畸变。人物的黑历史，不断地曝光，没有人是无辜的，每个人都是罪有应得的。
