---
uuid:             fc9172cb-8edd-4f5f-b275-b200c89a8c09
layout:           post
title:            电影：难为情
slug:             movie-life-is-peachy
subtitle:         null
date:             '2015-09-14T03:48:04.000Z'
updated:          '2015-09-14T03:48:04.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

昏昏欲睡，不知所云的电影，[豆瓣链接](http://movie.douban.com/subject/5268693/)

看着看着，忍不住，打开新窗口，便浏览网页便看，一心两用。

于是更不清楚了。

印象稍微深刻多便是，女主脑洞很大，女二初看起来不漂亮，越看越入眼。

结局当然也不懂。

只是，女二跑向女一，相拥，画面太美好了。
