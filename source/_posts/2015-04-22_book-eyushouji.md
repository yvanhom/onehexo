---
uuid:             265c99ac-89dd-45a2-927d-ec7b0bbd81fc
layout:           post
title:            读书：鳄鱼手记
slug:             book-eyushouji
subtitle:         null
date:             '2015-04-22T06:35:38.000Z'
updated:          '2015-07-31T07:01:56.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - reading

---


这本书中的一个情节一直在我的脑海里：

> “你应该不是被逼的，是自己选择不要别人对你失望的。”  
>  “你是要说，虽然不是我真心想要读这个东西，但还是为了我不想让别人失望这个目的，仍然出于‘我、的、意、愿’的选择，是吗？”  
>  “让他们失望会怎样？”  
>  “问得好” “你能忍受你的家人对你的失望吗？”  
>  “打从我懂事以来，我慢慢地在让家人经验对我的失望，一块一块打破他们为我塑造的理想形象，虽然会带给他们痛苦，但如果不这样子，我牺牲自己躲在假的理想形象里，夜以继日地努力掩埋对他们的怨恨，带给他们的痛苦不见得较小。”  
>  “你把理想形象的每一块都打碎了吗？”  
>  “很难。辛苦打碎了某一块，双方都受到伤害，自己又会迎着他们构造的方法建造起新的一块，像是补偿，常常自乱阵脚。对他们总是有爱，也有起码被接受的需要，所以要很勇敢地把自己和他们分开，否则一临到要拿对他们的爱和需要做本钱，换得自己的自由时，总会在冲突的刀口上退却下来。”  
>  “我这真的叫不战而下。跟精神病患担心自己只要一动全世界的人都会死光，所以必须僵直不动，有些成分相同，是不是？”

书中，拉子知道自己是什么人，所以做得很果断，虽然打碎家人对自己的每一块幻想很困难，很辛苦，很痛苦，可是依然很决绝，因为自己是无论无何都无法让家里人满意，自己是特殊的人，边缘人群之一，勉强自己只会让自己去怨恨。

我一直都不知道自己是什么样的人，一直按照家里人，周围人对自己的期待而活着，记得以前写作文，还写过要努力让别人对自己不失望，活到二三十岁，才算是懂事，才问自己什么是真正的我，真正的我想要什么。

或许到了这个岁数，按照以前的套路活着，不闻不问，才不会“荒废岁月”，可是一经自我发问，便完全停不下来，只想要找到真正的我。

而真正的我是什么？

活了二三十年，三观都不明确，真是白活了。

回到《鳄鱼手记》，邱妙津描绘了拉子与水伶，吞吞和柔柔，梦生和楚狂，相爱却疏离，互相伤害，不被社会承认，或许注定自己的内心便不能安定，无法产生安全感，片刻的犹豫便做出伤害对方的事情。

最终还是没看明白这本书。

[豆瓣链接](http://book.douban.com/subject/11527635/)。



