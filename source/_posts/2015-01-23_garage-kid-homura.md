---
uuid:             63052641-3f4f-4ef9-a1b3-8702e25fe82e
layout:           post
title:            景品：丧服晓美焰
slug:             garage-kid-homura
subtitle:         null
date:             '2015-01-23T09:36:11.000Z'
updated:          '2016-07-04T07:41:37.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world
  - garage-kid

---


对吼姆拉情有独钟，尤其是丧服焰更是帅呆了，念念不忘留着梯子眼泪的吼姆拉。

这款景品，是几个月前订的，蛮便宜的，而且提前订跟买现货没啥差别。

收货之后，拆开，发现我焰学麻美学姐，是断头的，果断装上，为了让我焰站到地上，折腾了半天。

折腾完后，就拍照，阳光明媚，光线不对。

先来张光线没那么强的。

![](http://o9sak9o6o.bkt.clouddn.com/comics/homura-1.jpg)

被圆神圣光照耀的我焰。

![](http://o9sak9o6o.bkt.clouddn.com/comics/homura-2.jpg)

![](http://o9sak9o6o.bkt.clouddn.com/comics/homura-3.jpg)

来张俯视图，什么，仰视图，没有。  
![](http://o9sak9o6o.bkt.clouddn.com/comics/homura-4.jpg)

我焰表情淡定，猜不出在想什么。  
![](http://o9sak9o6o.bkt.clouddn.com/comics/homura-5.jpg)



