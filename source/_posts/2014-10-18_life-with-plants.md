---
uuid:             21f94399-e1c1-4936-92a1-85644ce4995d
layout:           post
title:            种花草，过生活
slug:             life-with-plants
subtitle:         null
date:             '2014-10-18T13:00:00.000Z'
updated:          '2016-07-06T03:33:37.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - plants

---


从最容易养的开始

![](http://o9sak9o6o.bkt.clouddn.com/plants/cactus.jpg)

同时也使用一下[贴图库](http://tietuku.com/)的外链功能。

===== 2016-07-06 更新 =====

贴图库的免费图库功能开始收费，每年 100。要收费没关系，可是刚收到通知就发现一些图片已经失效，显示“图片服务器异常，正在修复中”，修复了两天都没有修复完成，一直失效中。进贴图库页面管理，发现我所有的相册图片数目都是 0，几天后发现有旧版入口，进旧版，终于显示了我原有的图片，但是失效的图片比起前两天的多了很多。

真是伤透了心，所谓的永久免费承诺只是空话，就算运转困难，要收费也没关系，只是一要收费，就图片渐渐失效，太不厚道了，像是在威胁我，不交钱就再也拿不回图片。面对威胁，我是不会妥协的。还没失效的图片转到七牛云上，已经失效的，再找找看，看能不能补回来，补不回来的，就只能算了。
