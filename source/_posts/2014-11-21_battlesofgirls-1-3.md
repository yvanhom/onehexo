---
uuid:             4d4e54a1-fdfd-4cf2-b068-262a8251a513
layout:           post
title:            '少女们的战争 第一章 第三节 菲特'
slug:             battlesofgirls-1-3
subtitle:         null
date:             '2014-11-21T12:00:00.000Z'
updated:          '2015-07-31T06:42:35.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world
  - fanfiction
  - battlesofgirls

---


雪白的天花板，雪白的墙壁，雪白的床，一切都是那么的单调无聊，对比起来，窗外则是湛蓝的天空，漂浮不定的白云，而从天空中俯瞰，则可以看到生机勃发的多彩大地。

她本该属于那广阔的天空，自由自在地翱翔，为守护美丽的大地而战斗，可是现在，却不得不局限在一个小小的病房里，命运就是这么折腾她，夺去了她的翅膀，将她从高空中狠狠地打下去，与大地来个无情地接触。

从此，她将无法重返天空。莎玛尔是这么瞒着她对疾风说的，可她巧合地偷听到了，

难受无比，可是这都是自己自作自受，当自己还健康的时候，为什么没有好好珍惜，为什么那么莽撞，只会不断地全力全开，不断地透支自己的生命力。

为什么只有等到失去才后悔，才懂得珍惜？

可是世上却没有后悔药。

而现在也不是后悔的时候，她心底里还始终抱有一丝希望，健康的希望，魔导师的希望，自由的希望。

打起精神来，积极配合医师，按时做各种复健工作，与这具破败的身体抗衡。

只要精神不跨，跨了的身体也终有一天会恢复生机。

而且，这个身体也不止是自己的，也是父母的，自己住院的事还没让他们知道，下次见面，一定是健康的奈叶，而下次也不会太远。也是朋友们的，也是菲特的，自从自己住院，她们可都非常担心，每天都来探望，尤其是菲特，最近都憔悴了很多，看着比奈叶还没精神。

不久前，她们都还是见习执行官，经常一起行动，菲特比较洒脱帅气，在生活中也就比较不注重细节，而她也就经常照顾菲特，现在菲特一个人，肯定不好受。

菲特，今天是你执行官考试的日子，不知道结果如何了。一定不能因为奈叶而发挥失常。

奈叶抬头往门边望去，也差不多了。

无聊的养病度日，无尽的精神肉体抗争，在这样的生活中，奈叶唯一的欣慰便是她的朋友们来探望她，她们的温柔给了她力量，尤其是菲特，每次见面，她都像金色抱抱犬一样把她的头埋在奈叶胸前，奈叶抱着她，感受菲特身体传来的温度，然后感觉到自己的身体也充满了生机，只要一直抱下去，自己一定会康复的。

而菲特一般会在这个时候到，她的生物钟似乎已经记下了菲特到来的时间，当时间到来时，她便有所感觉。

这不，病房的门打开了，从门进来的正是那个金色双马尾的帅气女孩子。

犹豫地走进病房，菲特不知道怎么跟奈叶说，她的执行官资格考试又挂了。

她或许是时空管理局最年轻的见习执行官，表现一直优秀，上级对她通过考试成为最年轻的执行官没有任何怀疑，可是她现在已经挂了两次。

同样优秀，哦，不，是更加优秀的奈叶住进了医院，本来约好两人一起参加考试的，一起创造时空管理局的新记录。可是现在，只有菲特一个人。如果只有她一个人成为执行官，为了任务满世界跑，岂不是留奈叶一个人呆在医院，呆在家里。

她不能不想，分心，走神，于是在最后的实战考核中挂了。

菲特慢慢地走向奈叶，都怪自己不够坚强，还达不到执行官的资格，可是如果说给奈叶听，奈叶肯定会自责的。

没有奈叶在身边，她总是把事情搞砸。

奈叶已经知道她今天有考核了，瞒也瞒不住，不过可以先拖着。

菲特一靠近奈叶就一头扎进奈叶怀里，蹭啊蹭。菲特很陶醉其中，心想奈叶也是这样的，大家都醉了，别的事情就都不用理了。

奈叶被这么一抱，全身都有所感觉，然后又都舒服地软下去。

不能沦陷，菲特进来时犹豫的表情，犹豫的步伐，奈叶可没有错过，金色抱抱犬的想法太容易懂了。

奈叶轻轻地推开菲特的温柔攻击。

“菲特，怎么身上有伤？”

不愧是奈叶，都经过莎玛尔处理过了还能看出来。

“是啊，奈叶！执行官就算只是实习，也是非常危险的工作，不小心就挨了敌人一下。”

“希格诺没有手下留情？”

“是啊，希格姆那家伙明明可以见好就收，结果还多砍了几下…下……”

啊，菲特恨不得把说出来的话吞下去。希格诺把守执行官考试的最后一关，实战，只要撑过一段规定时间便算合格，撑得越久成绩越好，打败更是考生不敢想的，虽然希格诺也不会全力全开。

菲特有可能会是第一个达成奇迹的考生。

可是菲特两次考试，都没撑过合格时间。

这次看到菲特总是走神，各种误判， 希格诺生气地在菲特败后又多发了两招，实打实地招呼到菲特身上，疼得菲特差点晕过去。

“菲特，大家这么期待，你就这样，希格姆不多砍你几下怎么行！“

奈叶语气严厉，可是心里却是痛，当初明明约好的一起成为执行官，可是现在…

不能心软，不然菲特会荒废好长一段时间。

”奈叶，你看，执行官那么危险，东奔西跑，多累，我不如当个教导官吧…“

”菲特…“奈叶厉声道。

“菲特，我不希望你因为现在的我而被束缚住，能够看着你高飞，我也会很幸福的…“

“奈叶，我情个假，等你好了，我们再一起考试吧……”

“菲特！…”

看着菲特耸拉着脑袋，一副尽管说的”乖孩子“样，奈叶不知道该说什么，也知道就算说什么她也没听进去，于是转向门口。

”疾风，你也说菲特几句。“

跟在菲特后边的疾风，看到菲特一进门，两口子就放闪光，于是就没进门，好好观察，恨不得手上有DV机，没想到只闪光一会就训起来了。

正想着要赶紧溜才是，就被叫住，不愧是暴君…

疾风无奈，只能进门帮助数落。

“菲特，你也真是的，这种时候应该争创佳绩，来让奈叶开心才是…”

训话的最后，以菲特答应尽快，马上，立刻参加第三次执行官考试，全力全开作为结局。

不过菲特知道，现在的自己全力全开，只会是疯狂乱来，问题始终还是无法解决。

除非奈叶能够恢复健康。而这目前看来是多么的渺茫。

“如果奈叶能复原，一切就都没问题了。”离开医院的菲特，在去工作的路上无奈地低头叹气。

“如果是你的话，这样的愿望也是能够实现的。我可以帮你创造奇迹。“

顺着声音的源头，菲特侧身望去，一只耳朵长长的奇怪生物进入眼界，没有在这个世界见到过，让人感觉很可爱，只是血红色的眼睛似乎埋藏着不为人知的秘密。

”什么？“

“在某个世界中，存在着一个能够实现任何愿望的圣杯，而为争夺圣杯，被召唤的英灵和被选中的魔术师参与圣杯战争。菲特·泰斯特罗莎·哈拉温，你是否要为了创造奇迹而参战？“



