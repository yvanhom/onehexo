---
uuid:             40b6b1e5-b994-431f-af76-fee9c6552c7c
layout:           post
title:            学做饭，过生活
slug:             life-with-cooking
subtitle:         null
date:             '2014-10-19T13:00:00.000Z'
updated:          '2016-07-04T07:14:14.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - foods

---


材料：皮蛋，瘦肉，菠菜，葱，姜，盐，鸡精

做法：

- 米泡一会
- 水煮开，加米，煮熟，搅啊搅
- 皮蛋切开，瘦肉切好，加点盐
- 加皮蛋，瘦肉，姜，拌啊拌
- 加菠菜，葱，拌啊拌
- 加盐，鸡精，拌啊拌
- 然后就好了

于是成果如下：

![](http://o9sak9o6o.bkt.clouddn.com/cooks/pidanshourouzhou.jpg)

一锅三碗，每碗如下：

![](http://o9sak9o6o.bkt.clouddn.com/cooks/pidanshourouzhou-2.jpg)

吃起来感觉还是太淡了，菜太多了，下次改进

瘦肉口感不错，味道也不错 做顿粥真麻烦，感觉弄得到处都是，晚上得大扫除一下



