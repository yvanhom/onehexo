---
uuid:             78b35894-8a00-40ab-bd99-00b7ed02eef4
layout:           post
title:            终于让馒头不开口了
slug:             bun-without-a-smile
subtitle:         null
date:             '2014-12-03T16:00:00.000Z'
updated:          '2016-07-06T03:36:54.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - foods

---


上次做馒头，馒头笑开了口。

这次吸取经验，把所有的裂缝都埋到底部，而不是馒头上表面。

果然有用。

蒸前，馒头表面很光滑。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/mantou-1.jpg)

蒸后，表面没那么光滑，不过也没开口了。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/mantou-2.jpg)



