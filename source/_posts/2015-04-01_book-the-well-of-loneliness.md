---
uuid:             c29ca4f8-cf71-4ea4-ae1a-5517863f4774
layout:           post
title:            读书：孤寂深渊
slug:             book-the-well-of-loneliness
subtitle:         null
date:             '2015-04-01T22:13:35.000Z'
updated:          '2015-07-31T07:01:24.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - reading

---


[寒假读书清单](https://one.yvanhom.com/2015/01/book-booklist/ "读书：春节假期书单")中，列了好多书，结果只读了一本，还没读完，惭愧！

这本书真心不对我胃口，死命强迫自己去读，去读，结果读到90%了，还是放弃了，因为图书馆的还书时间到了，也不想赶完。

很差劲的一次阅读体验。

不知道翻译的原因占了多少原因，总之阅读完全不顺畅。

忧郁着要不要写读后感，还没看完没资格评价，可是还是写了，就算是记录下自己曾经尝试去读这本书吧。

不对胃口的地方：

- 总是出现一些看清楚却无多大作为的人来发感慨，表示对斯蒂芬的同情，先是父亲，然后是老师。
- 人的孤寂是不可避免的，也是他人所难以理解的，而书本便是要将一个人的孤寂传达给另一个人，可是我还是不能理解斯蒂芬的孤寂，以及在此孤寂下的所作所为。
- 情节发展吸引不住我，感觉太拖沓了。



