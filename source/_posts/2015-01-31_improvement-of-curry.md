---
uuid:             3cec69f2-03b4-4f66-bd4a-5464eba6bc8b
layout:           post
title:            吃喝：改进咖喱饭
slug:             improvement-of-curry
subtitle:         null
date:             '2015-01-31T09:35:08.000Z'
updated:          '2016-07-04T07:44:28.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - foods

---


自从[那次做咖喱饭](/2015/01/japanese-gali/ "日式咖喱饭")以来，又做了两三次，都没那次成功。

要么水太多，不够黏稠，要么胡萝卜没熟，要么土豆煮烂了都成土豆泥了。

于是我的胃，我的肚子一次又一次地接受锻炼，百毒不侵。

这次学到教训，土豆跟胡萝卜分开煮，先煮胡萝卜和猪肉，煮熟后，捞起，用碗盛起放到一边，再煮土豆。土豆熟后再加胡萝卜，猪肉，咖喱，搅拌。

这次用的咖喱是亚马逊的“[House 好侍苹果咖喱调味料-微辣230g(日本进口)](http://www.amazon.cn/gp/product/B00Q9M627S?psc=1&ref_=oh_aui_detailpage_o00_s00)”。

成功了，好吃。

不过一点都不辣，也没有尝到什么苹果味道。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/galifan-3.jpg)

