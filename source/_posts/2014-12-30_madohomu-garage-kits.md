---
uuid:             31c8cc29-ede5-4b75-a24f-e124a6160481
layout:           post
title:            龙精石的圆焰手办
slug:             madohomu-garage-kits
subtitle:         null
date:             '2014-12-30T09:32:31.000Z'
updated:          '2016-07-04T07:37:56.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world
  - garage-kid

---


历经大半年，淘宝上的龙精石圆焰手办终于到手。

感动得泪流满面。

以后神魔镇宅，诸事大吉。

质量可以接受。小圆不错，可是小焰就不满意了。脸部太凸，发型还原差，有些角度都快认不出来了。

不过CP一起的手办少，这么有爱的手办，我一定要收才行。

小焰，你的脸！

![](http://o9sak9o6o.bkt.clouddn.com/comics/madoka-1.jpg)

下面这张有爱！

![](http://o9sak9o6o.bkt.clouddn.com/comics/madoka-2.jpg)

各种角度：

![](http://o9sak9o6o.bkt.clouddn.com/comics/madoka-3.jpg)

![](http://o9sak9o6o.bkt.clouddn.com/comics/madoka-4.jpg)

![](http://o9sak9o6o.bkt.clouddn.com/comics/madoka-5.jpg)

![](http://o9sak9o6o.bkt.clouddn.com/comics/madoka-6.jpg)

![](http://o9sak9o6o.bkt.clouddn.com/comics/madoka-7.jpg)

![](http://o9sak9o6o.bkt.clouddn.com/comics/madoka-8.jpg)

下面这张也不错。

![](http://o9sak9o6o.bkt.clouddn.com/comics/madoka-9.jpg)

拍完后，发现忘了把连带的地毯铺上，罪过罪过。



