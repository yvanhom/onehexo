---
uuid:             647322c6-62d7-4952-afd4-f070c630bd5b
layout:           post
title:            电影：梦径
slug:             movie-yume-no-kayoiji
subtitle:         null
date:             '2015-09-04T21:30:06.000Z'
updated:          '2015-09-04T21:30:06.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

日本电影，[豆瓣链接](http://movie.douban.com/subject/20530286/)

整部电影观完，太无聊了，不得不佩服自己居然看完了。

节奏慢，主线不清。

就是[竹富圣花](http://movie.douban.com/celebrity/1312829/)比较漂亮，想看看结果是怎么样。

60 年前的百合恋，结果一个生病离去，一个也紧随着离去，但是其中一个却作为鬼魂留在了学校，等待另一个的到来。

为什么不一起离去？

然后很像另一个的女主来了，鬼魂想带走女主。

女主一番纠结也想离去。

可是鬼魂犹豫了。

女主很坚定。

然后地震。

然后女主发现，父母很关心她，大家很在意她。

不能离去，活下去很美好。

莫名其妙，看到想困。

理解不了你们的哀伤。
