---
uuid:             12eeca02-bd35-4d5c-b6fc-4c7045d150b6
layout:           post
title:            '电影：零 ~Zero~ 剧场版'
slug:             movie-zero
subtitle:         null
date:             '2015-03-06T06:21:26.000Z'
updated:          '2015-07-31T06:58:36.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---


日本恐怖电影，捧游戏《零》系列的大腿，可是跟游戏完全没关系，附上[豆瓣链接](http://movie.douban.com/subject/25870716/)。

我胆子小，这是少有的一部全程看完没有取下耳机不听部分场景配乐的恐怖电影，完全不恐怖。

里面的妹子颜都好，尤其是那张照片，真是深得我心，一致完全不恐怖，后来真人的戏越多，越觉得还是照片好看，那种完全不恐怖的恐怖感。

妹子们颜好，除了一个假小子脸，而这个不起眼的假小子居然是主角，真是万万没想到。

剧场什么的真是弱到不行，完全不想吐槽。

都是因为妹子们的颜才看完的。

不过还是有喜欢的场景。

妹子的恐怖照真心3D，无辜的妹子们亲吻照片，就跟亲吻真人一样，仿佛都能看到无辜妹子们双手搭在鬼魂妹子的肩上。

假小子为救被错当成鬼魂妹子的妹子时，KISS，看得我真心醉了，一下，两下，然后三下。

很美好吧？还想再多来几下吧？

最后，假小子毕业后离开追求人生理想，恐怖妹子留下，不舍地分离，并等待将来美好的重聚，创造美好的记忆。



