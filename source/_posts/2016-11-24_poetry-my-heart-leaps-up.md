---
uuid:             8c6b8084-3a7c-49d7-a168-17e56928464f
layout:           post
title:            英诗：我一见彩虹，心儿便跳荡不止
slug:             poetry-my-heart-leaps-up
subtitle:         null
date:             '2016-11-24T12:08:17.000Z'
updated:          '2016-11-25T03:09:58.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - poetry
  - 3d-world

---

**作者**：威廉·华兹华斯（William Wordsworth）

**译者**：杨德豫

**原文**

> My heart leaps up when I behold

> 　　A rainbow in the sky:

> So was it when my life began;

> So is it now I am a man;

> So be it when I shall grow old,

> 　　Or let me die!

> The child is father of the Man;

> And I could wish my days to be

> Bound each to each by natural piety.

**诗译**

> 我一见彩虹高悬天上，

> 　　心儿便跳荡不已：

> 从前小时候就是这样；

> 如今长大了还是这样；

> 以后我老了也要这样；

> 　　否则，不如死！

> 儿童乃是成人的父亲；

> 我可以指望：我一世光阴

> 自始自终贯穿着对自然的虔诚。

**感想**

这首诗，喜欢的是诗人一见彩虹的喜悦，犹如初恋，对大自然美景的喜爱，自小维持至今，也愿持续到老来之时，心灵仿佛受到了触动。
