---
uuid:             cd9f8f12-6ae4-416a-9ee9-ae22a732273c
layout:           post
title:            '动漫：读或死 OVA'
slug:             anime-rod-ova
subtitle:         null
date:             '2015-12-09T02:17:26.000Z'
updated:          '2015-12-09T05:49:36.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world

---

这部 OVA，我的评价是不错，虽然有很多吐槽的地方。

先吐槽几句。

大英图书馆，这组织究竟做了什么？什么都没做好吧，就是绘制了张 BOSS 基地的地形图，然后全部都交给主角队。然后图书馆的领导便这么看着主角队，在那里学 EVA 司令官装逼。真是无语了。

读子，The Paper，能力真是逆天了，操纸术真是近乎全能，纸张可防御，可攻击，还可以化物。作为一个书痴，脑袋里肯定有很多知识，作为削弱，OVA 中，读子的知识完全没有派上用场。真是可惜了。

伟人们一个个跑出来，结果都凶神恶煞了，无论是唐僧，一休，还是贝多芬。

唐僧师傅真是逆天了。取经哪还需要徒弟啊，自己一个人就搞定了。金箍棒，体术无双，筋斗云，喷火，还能像摩西，念经劈开河流。有师傅的精彩表现在前，后边的伟人略显逊色。对比 The Paper，师傅更加逆天。

萳茜，The Deep，御姐，好萌，后期把读子都掩盖过去了。能力是穿透，穿墙什么的不在话下。这个能力，刚开始以为很菜，越到后边越牛逼。可以部分穿透，可以越过防御直接攻击。甚至我都怀疑都成不死之身了，对战师傅时，通过身体被金箍棒穿透来限制金箍棒，并进一步抓住师傅，对战一休时，直接把一休心脏捏碎。再加上那么御，被征服了，我。

果然，帅气的女孩子太犯规了。

结局好悲剧，The Deep and The Paper 才是一对啊。

有用连接：

* [豆瓣链接](http://movie.douban.com/subject/1760628/)
* [B 站在线播放](http://www.bilibili.com/bangumi/i/1968/)


