---
uuid:             983d873b-f229-4856-b5ff-4e589b1c9c84
layout:           post
title:            开始发芽的小兔子
slug:             gemmating-rabbit
subtitle:         null
date:             '2014-11-30T12:00:00.000Z'
updated:          '2016-07-04T07:23:30.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - plants

---


前阵子由于店家发错货，发了不贵的小兔子种子，盆，土过来，然后就不要意思地收下了。

看小兔子的说明，成长温度15~25摄氏度。

我这里现在都10度一下了，估计成活没望。

不过顽强的碧光环，还是发了芽。

希望它能撑过严寒，活到来年温暖的春天到来。

![](http://o9sak9o6o.bkt.clouddn.com/plants/xiaotuzi.jpg)



