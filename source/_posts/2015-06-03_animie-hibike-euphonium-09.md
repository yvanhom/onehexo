---
uuid:             7aaa45d6-13a4-4e7f-8153-cdb19ca27ed1
layout:           post
title:            动画：京吹，第九话，喜欢的一幕
slug:             animie-hibike-euphonium-09
subtitle:         null
date:             '2015-06-03T15:31:28.000Z'
updated:          '2016-07-04T08:03:11.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - hibike-enphonium
  - 2d-world

---


日等夜等，京吹的第九话终于出来了，等得好辛苦，真不知道当年宅们是怎么等京阿尼的“无尽的八月”的。

等来了，看完，真幸福，因为有以下这一幕：

![开始](http://o9sak9o6o.bkt.clouddn.com/comics/hibike-6.gif)

> I’m going to do my best, so you have to as well. Promise me.
> 
> I’ll do my best.

![结束](http://o9sak9o6o.bkt.clouddn.com/comics/hibike-7.gif)

心灵之友啊，好萌好萌。

借[tumblr 上的一句话](http://rachnera.tumblr.com/post/120541946145/she-sensed-her-soulmates-troubled-thoughts-and)来形容：

> she sensed her soulmate’s troubled thoughts and came to the rescue

她们的关系更进一步了。



