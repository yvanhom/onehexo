---
uuid:             917a3fab-d09a-4c01-8ee8-426f78b58bb9
layout:           post
title:            豆芽豆腐汤
slug:             bean-sprout-soup
subtitle:         null
date:             '2014-12-07T06:35:03.000Z'
updated:          '2016-07-04T07:27:19.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - foods

---


今天煮了两顿豆芽豆腐汤，一次是豆芽豆腐+猪肉汤，

![](http://o9sak9o6o.bkt.clouddn.com/cooks/douyadoufutang.jpg)

一次是豆芽豆腐+排骨汤。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/douyadoufutang.jpg)

味道还行，吃得很饱。



