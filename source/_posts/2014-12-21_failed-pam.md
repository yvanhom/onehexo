---
uuid:             8f61b83b-767b-4cdf-aefa-429ec35cd8bb
layout:           post
title:            失败的豆沙包
slug:             failed-pam
subtitle:         null
date:             '2014-12-21T09:32:36.000Z'
updated:          '2016-07-06T03:46:15.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - foods

---


不想再吃馒头了，于是买了点红豆，做起豆沙包。

本来想直接买现成的豆沙，可是在中百没找到，于是只能自己做了。

没有专用的捣碎器，用铁汤勺碾压，真心事倍功半，手都酸了，还是碾得不充分。

揉了四个豆沙包，小小的，感觉萌萌哒。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/doushabao.jpg)

可是蒸完后，不萌了，幻灭啊。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/doushabao-2.jpg)

吃起来只能说失败。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/doushabao-3.jpg)

不甜，包前尝过馅料，还是很甜的，不明白。

红豆沙还没到沙的地步。

吸取教训：

- 红豆需要再多煮会，不然太难碾碎了
- 用捣碎器捣，节省人工
- 多加点糖
- 多加点水，做得黏稠些

再接再厉！加油！



