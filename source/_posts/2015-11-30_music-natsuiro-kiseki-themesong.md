---
uuid:             c95f1c31-e897-4ca5-9660-c5289cfa7f7f
layout:           post
title:            '音乐：夏色奇迹 主题曲'
slug:             music-natsuiro-kiseki-themesong
subtitle:         null
date:             '2015-11-30T04:16:25.000Z'
updated:          '2015-11-30T04:17:56.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 2d-world
  - music

---

不久前看过动漫[夏色奇迹](/anime-natsuiro-kiseki/)，一直意犹未尽，想起那首时不时响起的主题曲，搜索了一番，很快便找到了。

“明日への帰り道”，翻译作“明日的回家路上”。

<iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=330 height=86 src="https://music.163.com/outchain/player?type=2&id=29966711&auto=0&height=66"></iframe>

```
明日への帰り道
Album ：Non stop road/明日への帰り道
TV アニメ「夏色キセキ」ED テーマ
急に黙（だま）り込んで
君は手を繋いだ
茜（あかね）空に消えてく今日を
見送る長いかげぼうし
放課後は永遠みたいに
サヨナラをごまかしてたね
だけど心は
旅立ちの予感を知ってた
果てしない未来への
躊躇（ためら）いを隠（かく）して
微笑む君の夢が眩しいよ
あと少しの季節を
惜（お）しむようにはしゃいだ
明日への帰り道
きっと１０年後は
何気（げ）ない日常
たとえ違う空の下でも
僕らは大丈夫だよね？
手を振った交差点から
家までの短い坂を
振り向きながら
小さくなる背中、遠くて
忘れないと誓った
子供っぽい約束
真剣な君の顔がうれしくて
大切に焼きつける
今がいつか彼方で
思い出に変わっても
果てしない未来への
躊躇（ためら）いを隠して
微笑む君の夢が眩しいよ
あと少しの季節を
惜しむようにはしゃいだ
明日への帰り道
きっと１０年後は
何気ない日常
たとえ違う空の下でも
僕らは大丈夫だよね
僕らはひとりじゃないんだ
```


