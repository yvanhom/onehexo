---
uuid:             7af4fbb3-f66d-4c72-b41f-02dff6621ecd
layout:           post
title:            哭，垂死的绿萝
slug:             scindapsus-aureus-in-danger
subtitle:         null
date:             '2014-11-26T12:00:00.000Z'
updated:          '2016-07-04T07:22:01.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - plants

---


两三天前刚到时，还很浓绿，生机，见[这里](http://life.ywxie.cf/?e=9)。

可是现在生机不再，是天气太冷？是我水培的方法不对？不知道怎么办。

只能老办法了，大动剪刀，将软趴趴的根页全部剪掉，留下还坚挺的部分。

希望能恢复生机。

加油。

剪过之后，不忍直视，看一次心疼一次，祈祷能停过这段艰难的时段。

![](http://o9sak9o6o.bkt.clouddn.com/plants/lvluo.jpg)



