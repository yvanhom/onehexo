---
uuid:             6b23bde2-1ef2-4db1-a3e6-0e03fa6ee341
layout:           post
title:            电影：脑浆炸裂少女
slug:             movie-brain-fluid-explosion-girl
subtitle:         null
date:             '2015-09-04T21:46:45.000Z'
updated:          '2016-07-04T08:29:58.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

日本中二片，[豆瓣链接](http://movie.douban.com/subject/26274132/)

看完，点上一百个赞，对上电波，喜欢得不得了。

喜欢女二稻泽花，聪明，果敢，善良，忧伤，黑起来也很帅。

喜欢女主市立花，颜值高，关键时刻有保障，值得信赖。

最后一幕。

> 市立花：我一定要让你恢复到原来的样子。因为我们休戚与共。

> 稻泽花：无所谓，我只想吃马卡龙。

![脑浆炸裂最后一幕](http://o9sak9o6o.bkt.clouddn.com/movies/naojiangzhalieshaonv.jpg)


