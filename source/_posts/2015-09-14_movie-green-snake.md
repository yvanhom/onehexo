---
uuid:             b6f068f0-993d-4266-8a1b-b9bb74b2216c
layout:           post
title:            电影：青蛇
slug:             movie-green-snake
subtitle:         null
date:             '2015-09-14T07:39:41.000Z'
updated:          '2015-09-14T07:39:41.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

千年的白蛇，五百年的青蛇。

人间情爱。

一个不懂，一个太懂。

到最后都懂了，只是悲剧已经发生。

人间有情，其实妖之间又岂会没有情，白蛇青蛇五百年的修行相伴，怎么会没有情。

情到深处最伤人。

虽然许仙是因为不想再看到白蛇青蛇受伤害，希望法海住手才出家的，可是这却也不能弥补他的无能。保护不了心爱的人，出家渴求停战，也只是逃避。这时候的他是没有路走的，只能可怜他。作为平凡的人，爱上不平凡的人，神话中的奇迹可不一定会发生。

蛇妖真心迷惑人。

凡夫俗子也只能被迷惑而沉沦了。
