---
uuid:             7154bac9-210e-475b-91a7-1dbaf19d9729
layout:           post
title:            电影：调查者
slug:             movie-the-investigator
subtitle:         null
date:             '2015-09-14T03:15:21.000Z'
updated:          '2015-09-14T03:15:21.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

[豆瓣链接](http://movie.douban.com/subject/2081865/)

20世纪90年代的英国，加入军队，纪律之中便有不能搞同性恋。

可是，军队中一半多女性是 Lesbian。

这可怎么办？

性别取向又岂是那么容易便能改变。

人变弯，又岂是一个纪律所能拉直的。

只能偷偷来，不留一丝痕迹，做好身边所有东西都会被搜查的准备。

偷偷摸摸的甜蜜，无情的搜查和逼问，交织而行。

想要留下什么东西纪念你，想要把你的某些物品留在身边。

是情。

也留下了证据。

逼问中，用谎言遮盖，说了一句谎话，便需要补上十句，一百句。

没完没了。

可是不得不，要保护自己，要保护她。

只能将自己所有的才能用在编织谎话上。

为什么不离开军队？

因为已经在军队待了那么多年。离开，还能干什么？

有时我自己也会想，我在学校学了那么多年，离开学校我能干什么？

不过，离开总是比留下来好。

因为，外面多世界更开阔。

离开了军队，也是能活下去了，人，总是能活下去的。
