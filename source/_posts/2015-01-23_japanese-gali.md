---
uuid:             2ad99e1e-50e8-40ea-9083-b954dc9e0523
layout:           post
title:            日式咖喱饭
slug:             japanese-gali
subtitle:         null
date:             '2015-01-23T16:10:25.000Z'
updated:          '2016-07-04T07:42:48.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - foods

---


看说明，简单的咖喱饭很容易做，于是在亚马逊上买了爱思必金牌咖喱一盒，加上菜市场的土豆，胡萝卜，猪肉，动起手来。

昨天做得并不好，首先是饭水下得太少，偏硬，其次煮咖喱时水太多，太稀，而且土豆和胡萝卜煮得不够熟。

今天晚餐吸取教训。

饭先煮。

在煮饭的同时，削土豆，切块，胡萝卜也切块，蒜切碎，一起炖。

估摸着差不多，猪肉，姜也下去一起炖，同时熟是最好的了。

炖熟后熄火加咖喱块，咖喱块很快融化，搅拌。

再开火加热一会就搞定了一锅咖喱，够黏稠。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/galifan.jpg)

此时饭也煮好了几分钟，取饭，浇上咖喱，开吃。

![](http://o9sak9o6o.bkt.clouddn.com/cooks/galifan-2.jpg)

好吃，然后不小心吃撑了，饭做太多了。



