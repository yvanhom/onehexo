---
uuid:             2e20aa90-c1ee-4d94-aa33-a99f0e99f7d1
layout:           post
title:            电影：少女特工队
slug:             movies-debs
subtitle:         null
date:             '2015-11-24T00:09:49.000Z'
updated:          '2015-11-24T00:09:49.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - 3d-world
  - movies

---

太搞笑，太闪了，[豆瓣链接](http://movie.douban.com/subject/1309039/)。

这是第二次看了，记得以前看过一次，不过已经忘得差不多，这次再看，忍不住从头笑到尾。

让人闻风丧胆的罪犯露茜，与之交手的特工无一生还，事实真相是她恋爱被甩后跑极地躲起来，跟踪而去的特工都冻死了，回来后走上了盲约的道路，遇见了少女特工艾米，开始了追女的路程。

无厘头搞笑。

全美国特工都在找被绑架的艾米，结果艾米在跟露茜开心地约会；抓奸在床的画面太美好，都忘了尴尬的气氛了；校长也好逗，拉着艾米，第一个跟露茜交手后活下来的特工，拍照，胡扯。

哈哈哈……

我已经乐疯了，大家不用管我。
