---
uuid:             95ea47d7-b7eb-4c19-aeda-9f88bb3df12e
layout:           post
title:            不一样的黑琴
slug:             unusual-cp
subtitle:         null
date:             '2015-03-19T07:50:53.000Z'
updated:          '2015-07-31T07:00:23.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags: null

---



# 介绍

黑子太可爱了，痴女太心水了。

可是也太可怜了，爱上一个只能当最好朋友的炮姐。

这篇文章便想要让黑子威武一些，给炮姐多吃点苦头。

基于黑子无敌论无下限脑洞。


# 目录

1. 相见 [1](/unusualcp-1-1/ "不一样的黑琴 1 相见")
2. 相识 [2](/unusualcp-2/ "不一样的黑琴 2 相识")
3. 相知 [3-1](/unusualcp-3-1/ "不一样的黑琴 3 相知（上）") [3-2](/unusualcp-3-2/ "不一样的黑琴 3 相知（中）") [3-3](/unusualcp-3-3/ "不一样的黑琴 3 相知（下）")
4. 相爱 [4-1](/unusualcp-4-1/ "不一样的黑琴 4 相爱（上）") [4-2](/unusualcp-4-2/ "不一样的黑琴 4 相爱（中）") [4-3](/unusualcp-4-3/ "不一样的黑琴 4 相爱（下）")
5. 相守 [5-1](/unusualcp-5-1/ "不一样的黑琴 5 相守（上）") [5-2](/unusualcp-5-2/ "不一样的黑琴 5 相守（中）") [5-3](/unusualcp-5-3/ "不一样的黑琴 5 相守（下）")



